## Usage
This tool prints back passed options and some option-based information.

      -h, --help          prints help message
      -v, --version       prints pre-defined version
      -V, --value=VALUE   takes optional parameter, prints it if passed
      -L, --list [LIST]   takes multiple comma-separated required parameters
      
Tool returns `exit_code 0` if finished successfully, `exit_code 1` otherwise.