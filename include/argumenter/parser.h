#if !defined(ARGUMENTER_PARSER_H_)
#define ARGUMENTER_PARSER_H_

namespace argumenter {

class Parser
{
  public: 
    const char* version = "0.2";
    int Parse (int, char**);
    Parser();
    Parser(char*);
  private:
    const char* kShortOpts = ":hVv::L:";
    char* program_name = "undefined";
    enum exitCodes 
    {
      SUCCESS,
      FAILURE
    };
    struct bArgumentsPassed
    {
      bool help, version, value, list; 
    } passed;
    void helpMessage(int);
    void versionMessage();
    void valueMessage(char*);
    void listMessage(char*);
    void duplicateKey(char);
};

} // namespace argumenter

#endif 