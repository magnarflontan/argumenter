#include "argumenter/parser.h"

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>

namespace argumenter {

const struct option kLongOpts[5] = {
  {"help",    no_argument,       0, 'h'},
  {"version", no_argument,       0, 'V'},
  {"value",   optional_argument, 0, 'v'},
  {"list",    required_argument, 0, 'L'},
  {0, 0, 0, 0}
};

Parser::Parser() 
{
  Parser::passed = { false, false, false, false };
}

Parser::Parser(char* name)
{
  Parser::passed = { false, false, false, false };
  Parser::program_name = name;
}

int Parser::Parse(int argc, char** argv)
{
  int index = 0;
  int arg;
  if (argc <= 1) {
    helpMessage(1);
    return 1;
  }
  while ((arg = getopt_long(argc, argv, kShortOpts, kLongOpts, &index)) >= 0) {
    switch (arg) {
      case 'h':
        if (!this->passed.help) {
          helpMessage(0);
          this->passed.help = true;
        } 
        else {
          duplicateKey(arg);
        }
        break;
      case 'V':
        if (!this->passed.version) {
          versionMessage();
          this->passed.version = true;
        } 
        else {
          duplicateKey(arg);
        }
        break;
      case 'v':
        if (!this->passed.value) {
          valueMessage(optarg);
          this->passed.value = true;
        } 
        else {
          duplicateKey(arg);
        }
        break;
      case 'L':
        if (!this->passed.list) {
          listMessage(optarg);
          this->passed.list = true;
        } 
        else {
          duplicateKey(arg);
        }
        break;
      case ':':
        fprintf(stderr, "Missing argument for flag %c!\n", optopt);
        return 1;
      case '?': default: {
        fprintf(stderr, "Unrecognized argument %c\n", optopt);
        helpMessage(1);
			  return 1;
      }
    }
  }
  return 0;
}

void Parser::helpMessage(int exitCode)
{
  if (exitCode != exitCodes::SUCCESS) {
    fprintf(stderr, "Try %s -h (--help) for more information\n", 
            program_name);
  }
  else {
    printf("Usage: %s [OPTIONS]\n", program_name);
    printf(
      "Echoes passed options and returns option-based information.\n \
      -h, --help          prints this message\n \
      -v, --version       prints version\n \
      -V, --value=VALUE   interprets passed message as string value, prints it\n \
      -L, --list [LIST]   takes multiple comma-separated values\n\n \
      Returns 0 if finished successfully, 1 otherwise.\n\r");
  }
} 

void Parser::versionMessage()
{
  printf("%s %s version\n", program_name ,version);
}

void Parser::valueMessage(char* arg)
{
  if (arg) {
    if (arg[0] == '=') {
      arg++;
    } 
    printf("Got value %s\n", arg);
  }
  else {
    printf("Empty value passed\n");
  }
}

void Parser::listMessage(char* arg)
{
  printf("Got list, representing as char array: %s\n", arg);
}

void Parser::duplicateKey(char arg)
{
  fprintf(stderr, "\033[0;31mWarning: duplicate key -%c!\033[0m\n", arg);
}

} //namespace argumenter