#include "argumenter/parser.h"

int main(int argc, char** argv) 
{
    auto parser = argumenter::Parser(argv[0]);
    return parser.Parse(argc, argv);
};